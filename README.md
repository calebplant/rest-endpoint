# Account REST API

## Overview

A REST endpoint for retrieving Account data from Salesforce and returning it to an external system. User provides the Account Id, Name, and desired returned fields (must be >= 10 fields). Body is returned in JSON format, with errors providing insight into what went wrong.

## Demo (~1 min)

[Quick demo showing some calls/responses sent via Postman](https://www.youtube.com/watch?v=8wpL72jC46U)

## Sample Calls

### Happy Case
Valid parameters
```
.../services/apexrest/Account?accountId=0011h00000nqcFZAAY&accountName=My Account&returnedFields=Id,Name,OwnerId,AccountNumber,Site,Type,Industry,AnnualRevenue,Rating,Phone
```
Status: 200
```JSON
{
    "attributes": {
        "type": "Account",
        "url": "/services/data/v51.0/sobjects/Account/0011h00000nqcFZAAY"
    },
    "Id": "0011h00000nqcFZAAY",
    "Name": "My Account",
    "OwnerId": "0051h000005h43kAAA",
    "AccountNumber": "145123",
    "Site": "adsasd.com",
    "Type": "Prospect",
    "Industry": "Banking",
    "Rating": "Cold"
}
```

### Error Case

Too few fields passed
```
.../services/apexrest/Account?accountId=0011h00000nqcFZAAY&accountName=My Account&returnedFields=Id,Name,OwnerId,AccountNumber
```
Status: 400
```JSON
{
    "error": "Not enough fields passed. Please pass at least 10 fields in returnedFields. Number of fields passed: 4"
}
```

Missing required parameters
```
.../services/apexrest/Account?accountId=0011h00000nqcFZAAY&returnedFields=Id,Name,OwnerId,AccountNumber,Site,Type,Industry,AnnualRevenue,Rating,Phone
```
Status: 400
```JSON
{
    "error": "Required parameter missing! Required parameters: accountId, accountName, returnedFields"
}
```

### Warning Case

Some passed fields don't exist on Account
```
.../services/apexrest/Account?accountId=0011h00000nqcFZAAY&accountName=My Account&returnedFields=Id,Name,OwnerId,AccountNumber,Site,Type,Industry,AnnualRevenue,Rating,Phone,A,B,C
```
Status: 200
```JSON
{
    "warning": "The following fields do not exist on Account: A, B, C",
    "Rating": "Cold",
    "Industry": "Banking",
    "Type": "Prospect",
    "Site": "adsasd.com",
    "AccountNumber": "145123",
    "OwnerId": "0051h000005h43kAAA",
    "Name": "My Account",
    "Id": "0011h00000nqcFZAAY",
    "attributes": {
        "url": "/services/data/v51.0/sobjects/Account/0011h00000nqcFZAAY",
        "type": "Account"
    }
}
```
