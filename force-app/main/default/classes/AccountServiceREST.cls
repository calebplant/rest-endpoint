@RestResource(urlMapping='/Account/*')
global with sharing class AccountServiceREST {

    static final List<String> REQUIRED_GET_PARAMS = new List<String>{'accountId', 'accountName', 'returnedFields'};
    
    @HttpGet
    global static void getRecord() {
        System.debug('START /Account/getRecord');
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String accountId = req.params.get('accountId');
        String accountName = req.params.get('accountName');
        String returnedFields = req.params.get('returnedFields');
        String warning = '';
        Map<String, String> errorRes = new Map<String, String>();

        // Check required fields
        if(accountId == null || accountName == null || returnedFields == null) {
            errorRes.put('error', 'Required parameter missing! Required parameters: ' + String.join(REQUIRED_GET_PARAMS, ', '));
            res.responseBody = Blob.valueOf(JSON.serialize(errorRes));
            res.statusCode = 400;
            return;
        }

        // Check number of fields
        if(returnedFields.split(',').size() < 10) {
            errorRes.put('error', 'Not enough fields passed! Please pass at least 10 fields in returnedFields. '
            + 'Number of fields passed: ' + returnedFields.split(',').size());
            res.responseBody = Blob.valueOf(JSON.serialize(errorRes));
            res.statusCode = 400;
            return;
        }

        // Check passed fields' validity
        Map<String, sObjectField> fields = Schema.SObjectType.Account.fields.getMap();
        List<String> inaccessibleFields = new List<String>();
        List<String> invalidFields = new List<String>();
        List<String> queryFields = new List<String>();
        for(String eachField : returnedFields.split(',')) {
            // Field exists?
            if(!fields.containsKey(eachField)) {
                invalidFields.add(eachField.trim());
            }
            // Check read permission
            else if (!fields.get(eachField).getDescribe().isAccessible()) {
                inaccessibleFields.add(eachField.trim());
            }
            else {
                queryFields.add(eachField.trim());
            }
        }
        // Add warnings about field validity
        if(inaccessibleFields.size() > 0 || invalidFields.size() > 0) {
            warning += invalidFields.size() > 0 ? 'The following fields do not exist on Account: ' + String.join(invalidFields, ', ') : '';
            warning += inaccessibleFields.size() > 0 ? 'You do not have permission to access these fields: ' + String.join(inaccessibleFields, ', ') : '';
        }

        // Run query
        if(queryFields.size() > 0) {
            String query = 'SELECT ' + String.escapeSingleQuotes(String.join(queryFields, ',')) + ' FROM Account ' +
                            ' WHERE Id = :accountId AND Name = :accountName ' +
                            ' WITH SECURITY_ENFORCED LIMIT 1';
            System.debug('Query: ' + query);
            try {
                List<Account> result = Database.query(query);

                if(result.isEmpty()) {
                    errorRes.put('error', 'No Account found with Id \'' + accountId + '\' and Name \'' + accountName + '\'.' );
                    res.responseBody = Blob.valueOf(JSON.serialize(errorRes));
                    res.statusCode = 400;
                    return;
                }
                else {
                    res.responseBody = Blob.valueOf(JSON.serialize(result[0]));
                    res.statusCode = 200;
                }
    
                if(!String.isEmpty(warning)) {
                    res.responseBody = Blob.valueOf(appendProperty(JSON.serialize(result[0]), 'warning', warning));
                }
            } catch (Exception e) {
                errorRes.put('error', e.getMessage());
                res.responseBody = Blob.valueOf(JSON.serialize(errorRes));
                res.statusCode = 500;
            }
        }
        else {
            errorRes.put('error', 'No valid Account fields were passed in returnedFields.');
            res.responseBody = Blob.valueOf(JSON.serialize(errorRes));
            res.statusCode = 400;
        }

        System.debug('END /Account/getRecord');
    }

    private static String appendProperty(String resBody, String key, String value) {
        Map<String, Object> root = (Map<String, Object>) JSON.deserializeUntyped(resBody);
        root.put(key, value);
        return JSON.serialize(root);
    }
}
